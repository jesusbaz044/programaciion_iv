from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, asc
from sqlalchemy.orm import sessionmaker
#para crear la base de datos

engine = create_engine('sqlite:///diccionario.sqlite')

Base = declarative_base()


Session = sessionmaker(bind=engine)

session = Session()

class Tabla(Base):
    __tablename__ = 'dictionary'
    id = Column(Integer, primary_key=True)
    palabra = Column(String, default='palabra por defecto')
    significado = Column(String, default='significado por defecto')

    def __repr__(self):
        return self.palabra
        
Base.metadata.create_all(engine)

class Dicction:
    def __init__(self):
        self.filas = session.query(Tabla).all()
        self.opcion =int(input(
        '''     Menú 
        1)Agregar Palabra
        2)Editar Palabra
        3)Mostrar Palabra
        4)Buscar Significado
        5)Eliminar Palabra
        6)Salir\n'''
        ))

    def agregarpalabra(self):
        if self.opcion == 1:
            print("Ingrese la palabra: ")
            pala = input()
            print("Ingrese su significado: ")
            sign = input()
            nueva_palabra = Tabla(palabra= pala, significado=sign)
            session.add(nueva_palabra)
            session.commit()
            print("Se ha añadido correctamente\n")
    def editarpalabra(self):
        if self.opcion == 2:
            u = session.query(Tabla).order_by(asc(Tabla.id)).all()
            for count, item in enumerate(self.filas):
                print(f'{count + 1}. {item}\n')
            print("Ingrese el numero de la palabra que desea editar: ")
            editar = input()
            for c, i in enumerate(u):
                if int(editar) == c +1:
                    nueva_pala = input("Ingrese la palabra:\n")
                    nuevo_sign = input("Ingrese el significado: \n")
                    nueva_palab= Tabla(palabra= nueva_pala, significado= nuevo_sign)
                    session.delete(i)
                    session.commit()
                    session.add(nueva_palab)
                    session.commit()
                        
    def mostrarpalabras(self):
        if self.opcion == 3:
            for count, item in enumerate(self.filas):
                print(f'{count + 1}. {item}\n')
    def buscarsignificado(self):
        if self.opcion == 4:
            print("TODAS LAS PALABRAS\n")
            u = session.query(Tabla).order_by(asc(Tabla.palabra)).all()
            if len(u)> 0:
                for count, item in enumerate(u):
                    print(f'{count + 1}. {item}: {item.significado}')
            else:
                print("No hay Nada")


    def eliminarPalabra(self):
        if self.opcion ==5:
            u = session.query(Tabla).order_by(asc(Tabla.palabra)).all()
            for count, item in enumerate(self.filas):
                print(f'{count + 1}. {item}\n')
            print("Ingrese el numero de la palabra que desea eliminar: ")
            borrar = input()
            for c, i in enumerate(u):
                if int(borrar) == c +1:
                    session.delete(i)
                    session.commit()
            print("La palabra ha sido eliminada")
    def cerrar(self):
        if self.opcion == 6:
            print("Ofi bro hasta luego")
            exit()

while True:
    dic1 = Dicction()
    dic1.mostrarpalabras()
    dic1.editarpalabra()
    dic1.buscarsignificado()
    dic1.agregarpalabra()
    dic1.eliminarPalabra()
    dic1.cerrar()